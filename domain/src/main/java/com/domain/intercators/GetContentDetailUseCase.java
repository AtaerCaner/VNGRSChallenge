
package com.domain.intercators;


import com.domain.model.list.Hit;
import com.domain.repositories.ContentsRepository;
import com.domain.schedulers.ObserveOn;
import com.domain.schedulers.SubscribeOn;
import com.domain.views.GetContentDetailView;
import com.domain.views.View;

import javax.inject.Inject;

import rx.Observable;

public class GetContentDetailUseCase extends BaseUseCase<Hit> {
    private GetContentDetailView view;
    private String slug;


    @Inject
    public GetContentDetailUseCase(ContentsRepository repository, SubscribeOn subscribeOn, ObserveOn observeOn) {
        super(repository, subscribeOn, observeOn);
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    @Override
    protected Observable<Hit> buildUseCaseObservable() {
        return ((ContentsRepository) baseRepository).getContentDetail(slug);
    }

    @Override
    public void attachView(View view) {
        this.view = (GetContentDetailView) view;
    }

    public GetContentDetailView getView() {
        return view;
    }

}
