
package com.domain.intercators;


import com.domain.model.list.GetContentListResponse;
import com.domain.repositories.ContentsRepository;
import com.domain.schedulers.ObserveOn;
import com.domain.schedulers.SubscribeOn;
import com.domain.views.GetContentListView;
import com.domain.views.View;

import javax.inject.Inject;

import rx.Observable;

public class GetContentListUseCase extends BaseUseCase<GetContentListResponse> {
    private GetContentListView view;
    private String searchString;
    private String page;
    private String hitsPerPage;


    @Inject
    public GetContentListUseCase(ContentsRepository repository, SubscribeOn subscribeOn, ObserveOn observeOn) {
        super(repository, subscribeOn, observeOn);
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setHitsPerPage(String hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }

    @Override
    protected Observable<GetContentListResponse> buildUseCaseObservable() {
        return ((ContentsRepository) baseRepository).getContentList(searchString, page, hitsPerPage);
    }

    @Override
    public void attachView(View view) {
        this.view = (GetContentListView) view;
    }

    public GetContentListView getView() {
        return view;
    }

}
