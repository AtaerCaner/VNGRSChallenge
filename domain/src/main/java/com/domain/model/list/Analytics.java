
package com.domain.model.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Analytics {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("subClass")
    @Expose
    private String subClass;
    @SerializedName("class")
    @Expose
    private String _class;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("group")
    @Expose
    private String group;
    @SerializedName("division")
    @Expose
    private String division;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("season")
    @Expose
    private String season;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubClass() {
        return subClass;
    }

    public void setSubClass(String subClass) {
        this.subClass = subClass;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

}
