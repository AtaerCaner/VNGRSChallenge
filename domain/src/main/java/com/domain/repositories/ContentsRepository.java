package com.domain.repositories;

import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;

import rx.Observable;

/**
 * Created by AtaerCanerCelik on 30/10/2017.
 */

public interface ContentsRepository extends BaseRepository {
    Observable<GetContentListResponse> getContentList(String searchString, String page, String hitsPerPage);

    Observable<Hit> getContentDetail(String slug);

}
