package com.domain.presenters;

import com.domain.intercators.GetContentDetailUseCase;
import com.domain.intercators.GetContentListUseCase;
import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;
import com.domain.presenters.subscribers.BaseInteractionSubscriber;
import com.domain.views.GetContentDetailView;
import com.domain.views.GetContentListView;

import javax.inject.Inject;

public class ContentPresenter implements Presenter {
    private final GetContentListUseCase useCase;
    private final GetContentDetailUseCase contentDetailUseCase;


    public void attachGetContentListView(GetContentListView view) {
        useCase.attachView(view);
    }
    public void attachGetContentDetailView(GetContentDetailView view) {
        contentDetailUseCase.attachView(view);
    }

    @Inject
    public ContentPresenter(GetContentListUseCase useCase, GetContentDetailUseCase contentDetailUseCase) {
        this.useCase = useCase;
        this.contentDetailUseCase = contentDetailUseCase;
    }

    public void getContentList(String searchString, int page, int hitsPerPage) {
        useCase.setHitsPerPage(String.valueOf(hitsPerPage));
        useCase.setPage(String.valueOf(page));
        useCase.setSearchString(searchString);

        useCase.execute(new BaseInteractionSubscriber<GetContentListView, GetContentListResponse>(useCase.getView()) {
            @Override
            public void onNext(GetContentListResponse response) {
                ContentPresenter.this.useCase.getView().onGetContentListResponse(response);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ContentPresenter.this.useCase.getView().onError(e);
            }
        });
    }


    public void getContentDetail(String slug) {
        contentDetailUseCase.setSlug(slug);

        contentDetailUseCase.execute(new BaseInteractionSubscriber<GetContentDetailView, Hit>(contentDetailUseCase.getView()) {
            @Override
            public void onNext(Hit response) {
                ContentPresenter.this.contentDetailUseCase.getView().onGetContentDetailResponse(response);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ContentPresenter.this.contentDetailUseCase.getView().onError(e);
            }
        });
    }

    @Override
    public void destroy() {
        useCase.unsubscribe();
        contentDetailUseCase.unsubscribe();
    }
}
