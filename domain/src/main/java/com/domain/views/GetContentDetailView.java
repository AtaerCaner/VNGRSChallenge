package com.domain.views;


import com.domain.model.list.Hit;


public interface GetContentDetailView extends BaseInteractionView {
    void onGetContentDetailResponse(Hit response);

    void onError(Throwable throwable);
}
