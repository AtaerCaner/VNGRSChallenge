package com.domain.views;


import com.domain.model.list.GetContentListResponse;


public interface GetContentListView extends BaseInteractionView {
    void onGetContentListResponse(GetContentListResponse response);

    void onError(Throwable throwable);
}
