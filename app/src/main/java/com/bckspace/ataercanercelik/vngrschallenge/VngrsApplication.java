package com.bckspace.ataercanercelik.vngrschallenge;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.bckspace.ataercanercelik.vngrschallenge.di.ApplicationComponent;
import com.bckspace.ataercanercelik.vngrschallenge.di.ApplicationModule;
import com.bckspace.ataercanercelik.vngrschallenge.di.DaggerApplicationComponent;

/**
 * Created by AtaerCanerCelik on 30/10/2017.
 */

public class VngrsApplication extends MultiDexApplication {

    private ApplicationComponent applicationComponent;
    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public void initializeInjector(String token) {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this, token))
                .build();

    }


    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        initializeInjector("");

    }
}
