package com.bckspace.ataercanercelik.vngrschallenge.ui.detail;

import android.graphics.Paint;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bckspace.ataercanercelik.vngrschallenge.R;
import com.bckspace.ataercanercelik.vngrschallenge.constants.Contants;
import com.bckspace.ataercanercelik.vngrschallenge.ui.base.BaseActivity;
import com.bckspace.ataercanercelik.vngrschallenge.ui.detail.adapter.ContentOptionsAdapter;
import com.bckspace.ataercanercelik.vngrschallenge.util.Util;
import com.domain.model.list.Hit;
import com.domain.presenters.ContentPresenter;
import com.domain.views.GetContentDetailView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;

/**
 * Created by AtaerCanerCelik on 31/10/2017.
 */

@EActivity(R.layout.activity_content_detail)
public class ContentDetailActivity extends BaseActivity implements GetContentDetailView {
    private static final String TAG = "ContentDetailActivity";

    //region Injection
    @Inject
    ContentPresenter contentPresenter;
    // endregion

    //region View Binding
    @ViewById
    Toolbar toolbar;
    @ViewById
    ImageView imgContent;
    @ViewById
    TextView txtTitle, txtspecialPrice, txtPrice, txtQuantity;
    @ViewById
    RecyclerView recyclerOption;
    @ViewById
    Button btnMinus, btnPlus, btnAddtoBag;
    @ViewById
    ProgressBar pbImg;
    // endregion

    //region Extra
    @Extra
    String slug;
    // endregion

    //region Fields
    ContentOptionsAdapter contentOptionsAdapter;
    private int tempStockQty = 1;
    private int stockOfAllOption = -1;
    // endregion

    //region Initializing
    @AfterInject
    protected void init() {
        getApplicationComponent().inject(this);
        contentPresenter.attachGetContentDetailView(this);
    }

    @Override
    protected void initViews() {
        super.initViews();
        contentPresenter.getContentDetail(slug);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Util.changeViewEnability(btnAddtoBag, false);
        Util.changeViewEnability(btnMinus, false);


        contentOptionsAdapter = new ContentOptionsAdapter(this,
                () -> Util.changeViewEnability(btnAddtoBag, true));

        recyclerOption.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerOption.setAdapter(contentOptionsAdapter);
    }
    // endregion

    //region LifeCycle
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
    // endregion

    //region Network
    @Override
    public void onGetContentDetailResponse(Hit response) {
        contentOptionsAdapter.setOptionList(response.getConfigurableAttributes().get(0).getOptions());
        contentOptionsAdapter.notifyDataSetChanged();

        txtTitle.setText(response.getName());
        txtPrice.setText(Util.textToCurrency(response.getPrice().toString()));
        txtPrice.setPaintFlags(txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        if (response.getSpecialPrice() == null) {
            txtPrice.setVisibility(View.GONE);
            txtspecialPrice.setText(Util.textToCurrency(response.getPrice().toString()));
        } else {
            txtPrice.setVisibility(View.VISIBLE);
            txtspecialPrice.setText(Util.textToCurrency(response.getSpecialPrice().toString()));
        }

        Picasso.with(this).load(Contants.BASE_IMAGE_URL + response.getImage()).into(imgContent, new Callback() {
            @Override
            public void onSuccess() {
                pbImg.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });


        stockOfAllOption = response.getStockOfAllOptions().getHomeDeliveryQty();

        Util.changeViewEnability(btnPlus, stockOfAllOption != tempStockQty);

    }

    @Override
    public void onError(Throwable throwable) {
        Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show();
    }
    // endregion

    //region Click
    @Click
    protected void btnMinus() {
        if (tempStockQty == 2)
            Util.changeViewEnability(btnMinus, false);

        Util.changeViewEnability(btnPlus, true);

        tempStockQty--;
        txtQuantity.setText(String.valueOf(tempStockQty));

    }

    @Click
    protected void btnPlus() {
        if (tempStockQty == stockOfAllOption - 1)
            Util.changeViewEnability(btnPlus, false);

        Util.changeViewEnability(btnMinus, true);

        tempStockQty++;
        txtQuantity.setText(String.valueOf(tempStockQty));

    }

    @Click
    protected void btnAddtoBag() {
        Toast.makeText(this, "Item Added to Bag", Toast.LENGTH_SHORT).show();
    }


    // endregion
}
