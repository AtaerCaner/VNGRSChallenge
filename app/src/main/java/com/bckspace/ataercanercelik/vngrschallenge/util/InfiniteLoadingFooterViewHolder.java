package com.bckspace.ataercanercelik.vngrschallenge.util;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.bckspace.ataercanercelik.vngrschallenge.R;

/**
 * Created by AtaerCanerCelik on 31/10/2017.
 */

public class InfiniteLoadingFooterViewHolder extends RecyclerView.ViewHolder {

    public ProgressBar mProgress;

    public InfiniteLoadingFooterViewHolder(View itemView) {
        super(itemView);
        mProgress = (ProgressBar) itemView.findViewById(R.id.progressbar);

    }

    public void startAnimating(int listSize) {
        if (listSize == 0)
            mProgress.setVisibility(View.GONE);
        else
            mProgress.setVisibility(View.VISIBLE);
    }

    public void stopAnimating() {
        mProgress.setVisibility(View.GONE);
    }
}
