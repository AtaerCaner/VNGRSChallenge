package com.bckspace.ataercanercelik.vngrschallenge.di;


import android.content.Context;

import com.bckspace.ataercanercelik.data.cache.Cache;
import com.bckspace.ataercanercelik.data.cache.CacheImpl;
import com.bckspace.ataercanercelik.data.di.ActivityScope;
import com.bckspace.ataercanercelik.data.di.DataModule;
import com.bckspace.ataercanercelik.data.executor.JobExecutor;
import com.bckspace.ataercanercelik.data.repositories.ContentDataRepository;
import com.bckspace.ataercanercelik.vngrschallenge.UIThread;
import com.bckspace.ataercanercelik.vngrschallenge.VngrsApplication;
import com.domain.executor.PostExecutionThread;
import com.domain.executor.ThreadExecutor;
import com.domain.repositories.ContentsRepository;
import com.domain.schedulers.ObserveOn;
import com.domain.schedulers.SubscribeOn;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@ActivityScope
@Module(includes = DataModule.class)
public class ApplicationModule {
    private final VngrsApplication application;
    private final String token;

    public ApplicationModule(VngrsApplication application, String token) {
        this.application = application;
        this.token = token;
    }

    @Provides
    @ActivityScope
    String provideToken() {
        return token;
    }


    @Provides
    @ActivityScope
    Context provideApplicationContext() {
        return application;
    }

    /*@Provides @Singleton
    public Observable<A> provideA(){
        return Observable.defer(
                () -> Observable.just(new A())
        );
    }*/

    @ActivityScope
    @Provides
    public Picasso providePicasso() {
        return new Picasso.Builder(application).build();
    }

    @ActivityScope
    @Provides
    SubscribeOn provideSubscribeOn() {
        return (new SubscribeOn() {
            @Override
            public Scheduler getScheduler() {
                return Schedulers.newThread();
            }
        });
    }

    @ActivityScope
    @Provides
    ObserveOn provideObserveOn() {
        return (new ObserveOn() {
            @Override
            public Scheduler getScheduler() {
                return AndroidSchedulers.mainThread();
            }
        });
    }


    @Provides
    @ActivityScope
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @ActivityScope
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

    @Provides
    @ActivityScope
    Cache provideCacheImpl(CacheImpl cache) {
        return cache;
    }


    @Provides
    @ActivityScope
    public ContentsRepository provideContentDataRepository(ContentDataRepository repository) {
        return repository;
    }
}

