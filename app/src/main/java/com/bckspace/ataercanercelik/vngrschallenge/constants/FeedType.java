package com.bckspace.ataercanercelik.vngrschallenge.constants;

/**
 * Created by AtaerCanerCelik on 31/10/2017.
 */

public enum FeedType {
    FIRST_LOAD,
    PAGINATION
}
