package com.bckspace.ataercanercelik.vngrschallenge.ui.detail.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bckspace.ataercanercelik.vngrschallenge.R;
import com.bckspace.ataercanercelik.vngrschallenge.util.Util;
import com.domain.model.list.Option;

import java.util.List;

/**
 * Created by AtaerCanerCelik on 31/10/2017.
 */

public class ContentOptionsAdapter extends RecyclerView.Adapter<ContentOptionsAdapter.ContentOptionsAdapterViewHolder> {


    private List<Option> optionList;
    private OptionClickListener optionClickListener;
    private Context context;
    private int selectedPositon = -1;

    public interface OptionClickListener {
        void onOptionClick();
    }

    public class ContentOptionsAdapterViewHolder extends RecyclerView.ViewHolder {
        public Button btnOption;

        public ContentOptionsAdapterViewHolder(View view) {
            super(view);
            btnOption = view.findViewById(R.id.btnOption);
        }
    }


    public ContentOptionsAdapter(Context context, OptionClickListener optionClickListener) {
        this.context = context;
        this.optionClickListener = optionClickListener;
    }

    public void setOptionList(List<Option> optionList) {
        this.optionList = optionList;
    }


    @Override
    public ContentOptionsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_option_list, parent, false);
        ContentOptionsAdapterViewHolder holder = new ContentOptionsAdapterViewHolder(itemView);

        holder.btnOption.setOnClickListener(view -> {
            if (holder.getLayoutPosition() == selectedPositon)
                return;

            selectedPositon = holder.getLayoutPosition();
            notifyDataSetChanged();
            optionClickListener.onOptionClick();
        });

        return holder;


    }

    @Override
    public void onBindViewHolder(ContentOptionsAdapterViewHolder holder, int position) {

        Option option = optionList.get(position);

        holder.btnOption.setText(option.getLabel());

        if (position == selectedPositon){
            holder.btnOption.setBackgroundColor(ContextCompat.getColor(context, R.color.dark_gray));
            holder.btnOption.setTextColor(ContextCompat.getColor(context, R.color.white));
        }
        else {
            holder.btnOption.setBackground(ContextCompat.getDrawable(context, R.drawable.gray_border));
            holder.btnOption.setTextColor(ContextCompat.getColor(context, R.color.dark_gray));
        }

        Util.changeViewEnability(holder.btnOption, option.getIsInStock());






    }

    @Override
    public int getItemCount() {
        return optionList != null ? optionList.size() : 0;
    }


}
