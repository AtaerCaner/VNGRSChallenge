package com.bckspace.ataercanercelik.vngrschallenge;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.widget.ViewFlipper;

import com.bckspace.ataercanercelik.vngrschallenge.constants.FeedType;
import com.bckspace.ataercanercelik.vngrschallenge.ui.base.BaseActivity;
import com.bckspace.ataercanercelik.vngrschallenge.ui.detail.ContentDetailActivity_;
import com.bckspace.ataercanercelik.vngrschallenge.ui.main.adapter.ContentListAdapter;
import com.bckspace.ataercanercelik.vngrschallenge.util.RxSearch;
import com.domain.model.list.GetContentListResponse;
import com.domain.presenters.ContentPresenter;
import com.domain.views.GetContentListView;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;

import static com.bckspace.ataercanercelik.vngrschallenge.util.Util.hideKeyboard;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements GetContentListView {
    private static final String TAG = "MainActivity";

    //region Injection
    @Inject
    ContentPresenter contentPresenter;
    // endregion

    //region View Binding
    @ViewById
    SearchView searchView;
    @ViewById
    ViewFlipper viewFlipper;
    @ViewById
    RecyclerView recyclerView;
    // endregion

    //region Fields
    ContentListAdapter contentListAdapter;
    LinearLayoutManager mLayoutManager;
    protected int totalItemCount = 0;
    protected int firstVisibleItem = 0;
    protected int previousTotalItemCount = 0;
    protected int visibleItemCount = 0;
    protected boolean loading = true;
    final private int HITS_PER_COUNT = 15;
    private String lastSearchedText;
    private int pageCounter = 1;
    private FeedType feedType = FeedType.FIRST_LOAD;
    // endregion

    //region Initializing
    @Override
    protected void init() {
        super.init();
        getApplicationComponent().inject(this);
        contentPresenter.attachGetContentListView(this);
    }

    @Override
    protected void initViews() {
        super.initViews();

        contentListAdapter = new ContentListAdapter(this,
                slug -> {
                    hideKeyboard(MainActivity.this, searchView);
                    ContentDetailActivity_.intent(MainActivity.this).slug(slug).start();
                });

        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(contentListAdapter);
        viewFlipper.setDisplayedChild(3);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0)
                    hideKeyboard(MainActivity.this, searchView);

                visibleItemCount = recyclerView.getChildCount();

                firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                totalItemCount = contentListAdapter.getDataItemCount();

                if (loading && (totalItemCount > previousTotalItemCount)) {
                    loading = false;
                    previousTotalItemCount = totalItemCount;
                }

                int visibleThreshold = 2;
                if (totalItemCount > 2 && (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold))) {
                    loading = true;
                    pageCounter++;
                    contentListAdapter.startInfiniteLoading(true);
                    feedType = FeedType.PAGINATION;
                    contentPresenter.getContentList(lastSearchedText, pageCounter, HITS_PER_COUNT);
                    Log.e(TAG, "onScrolled: PAGINATION ACTIVE");
                }

            }
        });

        RxSearch.fromSearchView(searchView)
                .debounce(750, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(searchString -> {
                    if (searchString.trim().length() > 2) {
                        lastSearchedText = searchString;
                        viewFlipper.setDisplayedChild(0);
                        resetPagination();
                        contentPresenter.getContentList(searchString, pageCounter, HITS_PER_COUNT);
                    }
                });
    }
    // endregion

    //region Network
    @Override
    public void onGetContentListResponse(GetContentListResponse response) {
        if (response.getHits().size() == 0){
            viewFlipper.setDisplayedChild(2);
            contentListAdapter.startInfiniteLoading(false);
            return;
        }

        if (feedType == FeedType.FIRST_LOAD) {
            contentListAdapter.setContentList(response.getHits());
            contentListAdapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(0);
        } else {
            contentListAdapter.addContentToEnd(response.getHits());
        }

        viewFlipper.setDisplayedChild(1);
        contentListAdapter.startInfiniteLoading(false);

    }

    @Override
    public void onError(Throwable throwable) {

    }
    // endregion

    //region  LifeCycle
    @Override
    protected void onStart() {
        super.onStart();
        hideKeyboard(this, searchView);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard(this, searchView);
        finish();
    }
    // endregion

    //region Helper Methods
    protected void resetPagination() {
        previousTotalItemCount = firstVisibleItem = visibleItemCount = totalItemCount = 0;
        feedType = FeedType.FIRST_LOAD;
        pageCounter = 1;
        loading = true;
    }
    // endregion

}
