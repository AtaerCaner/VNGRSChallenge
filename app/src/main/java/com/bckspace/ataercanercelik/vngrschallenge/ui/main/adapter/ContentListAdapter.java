package com.bckspace.ataercanercelik.vngrschallenge.ui.main.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bckspace.ataercanercelik.vngrschallenge.R;
import com.bckspace.ataercanercelik.vngrschallenge.constants.Contants;
import com.bckspace.ataercanercelik.vngrschallenge.util.InfiniteLoadingFooterViewHolder;
import com.bckspace.ataercanercelik.vngrschallenge.util.Util;
import com.domain.model.list.Hit;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by AtaerCanerCelik on 31/10/2017.
 */

public class ContentListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_FOOTER = 0;
    private final int TYPE_NORMAL = 1;
    private boolean shouldLoadInfinite;

    private List<Hit> contentList;
    private ItemClickListener itemClickListener;
    private Context context;

    public interface ItemClickListener {
        void onItemClick(String slug);
    }

    public class ContentListAdapterViewHolder extends RecyclerView.ViewHolder {
        public TextView title, price, detail;
        public ImageView contentImage;
        public ProgressBar pbImg;

        public ContentListAdapterViewHolder(View view) {
            super(view);
            contentImage = view.findViewById(R.id.imgContent);
            pbImg = view.findViewById(R.id.pbImg);
            detail = view.findViewById(R.id.txtDetail);
            price = view.findViewById(R.id.txtPrice);
            title = view.findViewById(R.id.txtTitle);
        }
    }


    public ContentListAdapter(Context context, ItemClickListener itemClickListener) {
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    public void setContentList(List<Hit> contentList) {
        this.contentList = contentList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer_infinite_loading_view, parent, false);
            InfiniteLoadingFooterViewHolder vh = new InfiniteLoadingFooterViewHolder(v);
            LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            vh.itemView.setLayoutParams(layoutParams);

            return vh;
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_content_list, parent, false);
            ContentListAdapterViewHolder holder = new ContentListAdapterViewHolder(itemView);

            itemView.setOnClickListener(view -> {
                String slug = contentList.get(holder.getLayoutPosition()).getSlug();
                itemClickListener.onItemClick(slug);
            });

            return holder;

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof InfiniteLoadingFooterViewHolder) {
            InfiniteLoadingFooterViewHolder footerView = (InfiniteLoadingFooterViewHolder) holder;
            if (shouldLoadInfinite) {
                footerView.startAnimating(contentList.size());
            } else
                footerView.stopAnimating();
        } else {
            ContentListAdapterViewHolder contentListAdapterViewHolder = (ContentListAdapterViewHolder) holder;
            Hit hit = contentList.get(position);

            contentListAdapterViewHolder.title.setText(hit.getName());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                contentListAdapterViewHolder.detail.setText(Html.fromHtml(hit.getDescription(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                contentListAdapterViewHolder.detail.setText(Html.fromHtml(hit.getDescription()));
            }

            contentListAdapterViewHolder.price.setText(Util.textToCurrency(hit.getPrice().toString()));


            Picasso.with(context).load(Contants.BASE_IMAGE_URL + hit.getImage()).into(contentListAdapterViewHolder.contentImage, new Callback() {
                @Override
                public void onSuccess() {
                    contentListAdapterViewHolder.pbImg.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return contentList != null ? contentList.size() + 1 : 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position))
            return TYPE_FOOTER;
        else
            return TYPE_NORMAL;
    }

    public void addContentToEnd(List<Hit> list) {
        if (list == null || list.isEmpty())
            return;

        contentList.addAll(list);

        notifyDataSetChanged();
    }

    public int getDataItemCount() {
        if (contentList != null)
            return contentList.size();
        else
            return 0;
    }

    private boolean isPositionFooter(int position) {
        if (contentList != null)
            return position == contentList.size();
        else
            return position == 0;
    }

    public boolean isShouldLoadInfinite() {
        return shouldLoadInfinite;
    }

    public void startInfiniteLoading(boolean shouldLoadInfinite) {
        this.shouldLoadInfinite = shouldLoadInfinite;
        notifyItemChanged(getItemCount() - 1);
    }
}
