package com.bckspace.ataercanercelik.vngrschallenge.di;


import com.bckspace.ataercanercelik.data.di.ActivityScope;
import com.bckspace.ataercanercelik.vngrschallenge.MainActivity;
import com.bckspace.ataercanercelik.vngrschallenge.ui.base.BaseActivity;
import com.bckspace.ataercanercelik.vngrschallenge.ui.detail.ContentDetailActivity;

import dagger.Component;

@ActivityScope
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(BaseActivity activity);

    void inject(MainActivity activity);

    void inject(ContentDetailActivity activity);


}
