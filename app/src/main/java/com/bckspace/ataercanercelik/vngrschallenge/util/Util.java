package com.bckspace.ataercanercelik.vngrschallenge.util;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by AtaerCanerCelik on 31/10/2017.
 */

public class Util {
    private static final String currency = "AED";

    public static void hideKeyboard(Activity activity, SearchView searchView) {
        searchView.clearFocus();
        View v = activity.getWindow().getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static void changeViewEnability(View view, boolean isEnable) {
        view.setEnabled(isEnable);
        view.setClickable(isEnable);

        view.setAlpha(isEnable ? 1.0f : .4f);
    }



    public static String textToCurrency(String txt) {
        return String.format("%s %s", currency, txt);
    }
}
