package com.bckspace.ataercanercelik.vngrschallenge.ui.base;

import android.support.v7.app.AppCompatActivity;

import com.bckspace.ataercanercelik.vngrschallenge.VngrsApplication;
import com.bckspace.ataercanercelik.vngrschallenge.di.ApplicationComponent;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by AtaerCanerCelik on 30/10/2017.
 */

@EActivity
public abstract class BaseActivity extends AppCompatActivity {

    @AfterInject
    protected void init() {
        if (getApplicationComponent() != null)
            getApplicationComponent().inject(this);
    }

    @AfterViews
    protected void initViews() {

    }

    public ApplicationComponent getApplicationComponent() {
        return ((VngrsApplication) getApplication()).getApplicationComponent();
    }


}
