package com.bckspace.ataercanercelik.data.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.bckspace.ataercanercelik.data.cache.serializer.JsonSerializer;
import com.bckspace.ataercanercelik.data.exception.NetworkConnectionException;
import com.bckspace.ataercanercelik.data.exception.error403.AuthenticationException;
import com.bckspace.ataercanercelik.data.exception.error404.BadRequestException;
import com.bckspace.ataercanercelik.data.exception.error500.ServerException;
import com.domain.model.ErrorResponse;
import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;

public class RestApi {
    final String TAG = "RestApi";
    private final Context context;
    private final Service service;
    @Inject
    JsonSerializer serializer;


    //region Initialize
    @Inject
    public RestApi(Context context, Service endpoints) {
        this.context = context;
        this.service = endpoints;
    }

    private boolean isThereAnyError(Response response, Subscriber subscriber) {
        boolean isError = false;


        if (response == null) {
            subscriber.onError(new ServerException("Server Error"));
            return true;
        }

        if (response.errorBody() == null)
            return isError;

        ErrorResponse errorResponse = null;
        try {
            errorResponse = serializer.deserializeError(new String(response.errorBody().bytes()));


        } catch (IOException e) {
            e.printStackTrace();
        }

        if (errorResponse == null) {
            subscriber.onError(new ServerException("Server Error"));
            return true;
        }


        switch (response.code()) {
            case 400:
                isError = true;
                subscriber.onError(new BadRequestException());
                break;
            case 404:
                isError = true;
                subscriber.onError(new BadRequestException());
                break;
            case 401:
                isError = true;
                subscriber.onError(new AuthenticationException());
                break;
            case 500:
                isError = true;
                subscriber.onError(new ServerException());
                break;
        }


        return isError;
    }

    private boolean isThereInternetConnection(Subscriber subscriber) {
        boolean isConnected;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        if (!isConnected)
            subscriber.onError(new NetworkConnectionException("There are no internet connection !"));

        return isConnected;
    }
    // endregion

    //region Methods
    public Observable<GetContentListResponse> getContentList(final String searchString, final String page, final String hitsPerPage) {
        return Observable.create(new Observable.OnSubscribe<GetContentListResponse>() {
            @Override
            public void call(final Subscriber<? super GetContentListResponse> subscriber) {
                if (!isThereInternetConnection(subscriber))
                    return;

                try {

                    service.getContentList(searchString, page, hitsPerPage).enqueue(new Callback<GetContentListResponse>() {
                        @Override
                        public void onResponse(Call<GetContentListResponse> call, Response<GetContentListResponse> response) {
                            if (isThereAnyError(response, subscriber))
                                return;

                            subscriber.onNext(response.body());
                        }

                        @Override
                        public void onFailure(Call<GetContentListResponse> call, Throwable t) {
                            subscriber.onError(new NetworkConnectionException(t.getCause()));
                        }
                    });


                } catch (Exception e) {
                    subscriber.onError(new NetworkConnectionException(e.getCause()));
                }
            }
        });
    }

    public Observable<Hit> getContentDetail(final String slug) {
        return Observable.create(new Observable.OnSubscribe<Hit>() {
            @Override
            public void call(final Subscriber<? super Hit> subscriber) {
                if (!isThereInternetConnection(subscriber))
                    return;

                try {

                    service.getContentDetail(slug).enqueue(new Callback<Hit>() {
                        @Override
                        public void onResponse(Call<Hit> call, Response<Hit> response) {
                            if (isThereAnyError(response, subscriber))
                                return;

                            subscriber.onNext(response.body());
                        }

                        @Override
                        public void onFailure(Call<Hit> call, Throwable t) {
                            subscriber.onError(new NetworkConnectionException(t.getCause()));
                        }
                    });


                } catch (Exception e) {
                    subscriber.onError(new NetworkConnectionException(e.getCause()));
                }
            }
        });
    }
    // endregion





}

