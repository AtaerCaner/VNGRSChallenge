package com.bckspace.ataercanercelik.data.di;

import com.bckspace.ataercanercelik.data.net.Service;
import com.bckspace.ataercanercelik.data.net.ServiceGenerator;

import dagger.Module;
import dagger.Provides;


@Module
public class DataModule {

    @ActivityScope
    @Provides
    Service provideService() {
        return ServiceGenerator.createService(Service.class);
    }
}
