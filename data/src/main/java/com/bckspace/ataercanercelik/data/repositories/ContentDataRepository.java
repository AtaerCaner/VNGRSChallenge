package com.bckspace.ataercanercelik.data.repositories;

import com.bckspace.ataercanercelik.data.repositories.datasource.DataStoreFactory;
import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;
import com.domain.repositories.ContentsRepository;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by AtaerCanerCelik on 30/10/2017.
 */

public class ContentDataRepository implements ContentsRepository {
    private final DataStoreFactory dataStoreFactory;

    @Inject
    public ContentDataRepository(DataStoreFactory dataStoreFactory) {
        this.dataStoreFactory = dataStoreFactory;
    }

    @Override
    public Observable<GetContentListResponse> getContentList(String searchString, String page, String hitsPerPage) {
        return dataStoreFactory.createCloudDataStore().getContentList(searchString, page, hitsPerPage);
    }

    @Override
    public Observable<Hit> getContentDetail(String slug) {
        return dataStoreFactory.createCloudDataStore().getContentDetail(slug);
    }
}
