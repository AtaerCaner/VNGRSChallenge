package com.bckspace.ataercanercelik.data.repositories.datasource;


import com.bckspace.ataercanercelik.data.cache.Cache;
import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;

import rx.Observable;


public class DiskDataStore implements DataStore {

    private final Cache cache;

    public DiskDataStore(Cache cache) {
        this.cache = cache;
    }


    @Override
    public Observable<GetContentListResponse> getContentList(String searchString, String page, String hitsPerPage) {
        return null;
    }

    @Override
    public Observable<Hit> getContentDetail(String slug) {
        return null;
    }
}

