package com.bckspace.ataercanercelik.data.repositories.datasource;

import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;

import rx.Observable;


public interface DataStore {

    Observable<GetContentListResponse> getContentList(String searchString, String page, String hitsPerPage);

    Observable<Hit> getContentDetail(String slug);


}
