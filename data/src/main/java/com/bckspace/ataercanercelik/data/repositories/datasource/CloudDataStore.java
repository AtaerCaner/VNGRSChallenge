package com.bckspace.ataercanercelik.data.repositories.datasource;


import com.bckspace.ataercanercelik.data.cache.Cache;
import com.bckspace.ataercanercelik.data.net.RestApi;
import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;

import rx.Observable;

public class CloudDataStore implements DataStore {


    private final RestApi restApi;
    private final Cache cache;

    public CloudDataStore(RestApi restApi, Cache cache) {
        this.restApi = restApi;
        this.cache = cache;

    }


    @Override
    public Observable<GetContentListResponse> getContentList(String searchString, String page, String hitsPerPage) {
        return restApi.getContentList(searchString, page, hitsPerPage);
    }

    @Override
    public Observable<Hit> getContentDetail(String slug) {
        return restApi.getContentDetail(slug);
    }
}
