package com.bckspace.ataercanercelik.data.net;

import com.domain.model.list.GetContentListResponse;
import com.domain.model.list.Hit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Service {

    @POST("search/full/?")
    Call<GetContentListResponse> getContentList(@Query("searchString") String searchString, @Query("page") String page, @Query("hitsPerPage") String hitsPerPage);

    @GET("product/findbyslug/?")
    Call<Hit> getContentDetail(@Query("slug") String slug);


}
